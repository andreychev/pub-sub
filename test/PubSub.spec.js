import chai from 'chai';
import sinon from 'sinon';
import sinonChai from 'sinon-chai';
import PubSub from '../src/index.js';

chai.should();
chai.use(sinonChai);

describe("PubSub", () => {
    it("on and trigger", () => {
        let cb = sinon.spy();

        let pubSub = new PubSub();

        pubSub.on("new-event", cb);

        pubSub.trigger("new-event");

        cb.should.have.been.called;
    });

    it("off", () => {
        let cb = sinon.spy();

        let pubSub = new PubSub();

        pubSub.on("new-event", cb);

        pubSub.off("new-event");

        cb.should.have.not.been.called;
    });
});
